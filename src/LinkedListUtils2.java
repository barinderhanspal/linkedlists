
public class LinkedListUtils2 {


	public static Node addNodes(Node head, Node node){
		if(head == null)
			return node;
		Node current = head;
		while(current.next != null){
			current = current.next;
		}
		current.next = node;
		return head;
	}

	public static Node addNodesRec(Node head, Node node){

		if(head == null)
			return node;

		head.next = addNodesRec(head.next, node);
		return head;
		/*if(head == null)
			return node;
		else 
			head.next = addNodesRec(head.next, node);

		return head;*/
	}

	public static void printRec(Node head){
		if(head == null)
			return;
		System.out.print(head.data + "->");
		printRec(head.next);
	}

	public static void printList(Node head){
		Node current = head;
		while(current != null){
			System.out.print(current.data + "->");
			current = current.next;
		}
		System.out.println();
	}

	public static Node insertFirst(Node head, Node node){
		node.next = head;
		return node;
	}

	public static Node insertAt(Node head, Node node, int position){
		// position is >=0
		// position lies between the size of the list

		Node current = head;
		if(position == 0){
			node.next = head;
			return node;
		} else {
			while(position - 1 > 0 && current.next != null){
				current = current.next;
				position --;
			}
			node.next = current.next;
			current.next = node;
		}
		return head;
	}

	public static void printMiddle(Node head){
		Node current = head, skipper = head;

		while(skipper != null && skipper.next != null){
			skipper = skipper.next.next;
			current = current.next;
		}
		System.out.println(current.data);
	}

	public static void printNthFromLast(Node head, int n) throws Exception{


		/*	Node current = head, nthFromLast = head;
		while(current != null && (n-- >= 0)){
			current = current.next;
		}
		if(n >= 0)
			throw new Exception("out of bounds");
		while(current != null){
			current = current.next;
			nthFromLast = nthFromLast.next;
		}
		System.out.println(nthFromLast.data);*/
	}

	public static Node reverseIter(Node head){

		Node current = head, next = head, prev = null;

		while(current != null){
			next = current.next;
			current.next = prev;
			prev = current;
			current = next;
		}

		return prev;
	}

	public static Node reverseRec(Node trueHead, Node recHead){
		if(recHead.next == null || trueHead == null){
			trueHead = recHead;
			return trueHead;
		} else {
			trueHead = reverseRec(trueHead, recHead.next);
			recHead.next.next = recHead;
			recHead.next = null;
		}
		return trueHead;
	}

	public static boolean detectLoop(Node head){
		Node slowPtr = head, fastPtr = head;

		while(fastPtr != null && fastPtr.next != null){
			slowPtr = slowPtr.next;
			fastPtr = fastPtr.next.next;
			if(fastPtr == slowPtr){
				return true;
			}
		}
		return false;
	}

	public static Node removeDuplicates(Node head){
		Node current = head;

		while(current != null && current.next != null){
			if(current.data == current.next.data){
				current.next = current.next.next;
			} else {
				current = current.next;
			}
		}
		return head;
	}

	public static Node removeDuplicatesUnsorted(Node head){

		java.util.HashMap<Integer, Object> map = new java.util.HashMap<Integer, Object>();	
		Node current = head;
		if(current == null)
			return head;
		else 
			map.put(current.data, null);


		while(current != null && current.next != null){
			if(map.containsKey(current.next.data)){
				if(current.next.next != null){
					//current.next.data = current.next.next.data;
					current.next = current.next.next;
				} else {
					current.next = null;
				}
			} else {
				map.put(current.next.data, null);
				current = current.next;
			}

		}
		return head;





		/*java.util.HashMap<Integer, Node> nodeMap = new java.util.HashMap<Integer, Node>();
		Node current = head;
		while(current != null && current.next != null){
			if(nodeMap.containsKey(current.data)){
				if(current.next.next != null){
					current.data = current.next.data;
					current.next = current.next.next;
				} else {
					current.next = null;
				}
			} else {
				nodeMap.put(current.data, current);
				current = current.next;
			}
		}
		return head;*/
	}

	public static Node getIntersect(Node head1, Node head2){

		Node c1 = head1, c2 = head2, result = null;

		while(c1 != null && c2 != null){
			if(c1.data == c2.data){
				result = addNodes(result, new Node(c1.data));
				c1 = c1.next;
				c2 = c2.next;
			} else if(c1.data < c2.data){
				c1 = c1.next;
			} else {
				c2 = c2.next;
			}
		}

		return result;

		/*Node p1 = head1, p2 = head2;
		Node returnList = null;
		while(p1 != null && p2 != null){
			if(p1.data == p2.data){
				returnList = addNodes(returnList, new Node(p1.data));
				p1 = p1.next;
				p2 = p2.next;
				//System.out.println("add" + p1.data);
			} else if(p1.data < p2.data){
				p1 = p1.next;
			} else {
				p2 = p2.next;
			}
		}
		return returnList;*/
	}

	public static Node deleteAlternate(Node head){
		Node current = head;
		while(current != null && current.next != null){
			current.next = current.next.next;
			current = current.next;
		}
		return head;
	}

	public static void splitAlternate(Node head){
		Node a = null, b = null, current = head;

		while(current != null){
			a = addNodes(a, new Node(current.data));
			if(current.next != null){
				b = addNodes(b, new Node(current.next.data));
				current = current.next.next;
			} else {
				break;
			}
		}

		printList(a);
		printList(b);
	}

	public static Node sortMerge(Node head1, Node head2){
		Node p1 = head1, p2 = head2;
		Node ret = null;
		while(p1 != null || p2 != null){
			if(p1 != null && p2 != null){
				if(p1.data <= p2.data){
					ret = addNodes(ret, new Node(p1.data));
					p1 = p1.next;
				} else {
					ret = addNodes(ret, new Node(p2.data));
					p2 = p2.next;
				}
			}
			else if(p1 != null){
				ret = addNodes(ret, new Node(p1.data));
				p1 = p1.next;
			} else {
				ret = addNodes(ret, new Node(p2.data));
				p2 = p2.next;
			}
		}

		return ret;
	}

	public static boolean areIdentical(Node h1, Node h2){
		if(h1 == null && h2 == null)
			return true;
		else if ((h1 != null && h2 == null) || h1 == null && h2 != null)
			return false;
		return (h1.data == h2.data) && areIdentical(h1.next, h2.next);
	}

	public static Node reverseN(Node head, int n){
		Node current = head, prev = null, next = head;
		int count = n;
		while(current != null && count-- > 0){
			next = current.next;
			current.next = prev;
			prev = current;
			current = next;
		}

		if(next != null){
			head.next = reverseN(next, n);
		}

		return prev;
	}

	public static void main(String args[]) throws Exception{

		Node head = null, head2 = null;
		for(int i = 0; i < 7; i ++){
			head = addNodesRec(head, new Node(i));
			//head2 = addNodesRec(head2, new Node(i));
		}
		for(int i = 0; i < 7; i ++){
			head = addNodesRec(head, new Node(i));
			//head2 = addNodesRec(head2, new Node(i));
		}
		/*head = addNodesRec(head, new Node(0));
		head2 = addNodesRec(head2, new Node(1));
		head = addNodesRec(head, new Node(2));
		head2 = addNodesRec(head2, new Node(3));
		head = addNodesRec(head, new Node(4));
		head2 = addNodesRec(head2, new Node(5));
		head = addNodesRec(head, new Node(6));
		head2 = addNodesRec(head2, new Node(7));
		 */
		printList(head);
		//head = insertFirst(head, new Node(-1));
		//printList(head2);

		//Node t = null;
		//t = sortMerge(head, head2);
		//splitAlternate(head);
		//head = deleteAlternate(head);
		head = removeDuplicatesUnsorted(head);
		//	head = reverseN(head, 3);
		printList(head);
		//head = insertAt(head, new Node(10), 1);
		//printList(getIntersect(head, head2));
		//printNthFromLast(head, 6);
		//head = reverseRec(head, head);
		//head = removeDuplicatesUnsorted(head);
		//printList(head);
		//printMiddle(head);

	}
}
