
public class LinkedListUtils {

	public static Node getMiddle(Node head){
		Node current = head, fastPtr = head;
		while(fastPtr != null && fastPtr.next != null){
			current = current.next;
			fastPtr = fastPtr.next.next;
		}
		return current;
	}

	public static Node findNthLastNode(Node head, int n){
		Node current = head;
		Node nthLast = head;

		while(current != null){
			current = current.next;
			if(n <= 0){
				nthLast = nthLast.next;
			}
			n--;
		}
		return nthLast;
	}

	public static Node findNthLastNode2(Node head, int n){
		Node current = head;
		while(current != null && n > 0){
			current = current.next;
			n--;
		}

		if(n > 0) ;//exception.. out of bounds

		Node nthLast = head;
		while(current != null){
			current = current.next;
			nthLast = nthLast.next;
		}
		return nthLast;
	}

	public static Node addNode(Node head, Node newNode){
		if(head == null)
			head = newNode;
		else{
			Node current = head;
			while(current.next != null)
				current = current.next;
			current.next = newNode;
		}
		return head;
	}
	/*
	public static Node reverseIter(Node head){

		Node prev = null, current = head, next = head;

		while(current != null){
			next = current.next;
			current.next = prev;
			prev = current;
			current = next;
		}

		head = prev;
		return head;
	}
	 */

	public static Node mergeList(Node head1, Node head2){
		Node result = null, current1 = head1, current2 = head2, lastPointerResult = null;

		while(current1 != null && current2 != null){
			if(current1.data < current2.data){
				if(result == null){
					result = new Node(current1.data);
					lastPointerResult = result;
				} else {
					lastPointerResult.next = new Node(current1.data);
					lastPointerResult = lastPointerResult.next;
				}
				current1 = current1.next;
			} else {
				if(result == null){
					result = new Node(current2.data);
					lastPointerResult = result;
				} else {
					lastPointerResult.next = new Node(current2.data);
					lastPointerResult = lastPointerResult.next;
				}
				current2 = current2.next;
			}
		}

		while(current1 != null){
			lastPointerResult.next = new Node(current1.data);
			lastPointerResult = lastPointerResult.next;
			current1 = current1.next;
		}

		while(current2 != null){
			lastPointerResult = new Node(current2.data);
			lastPointerResult = lastPointerResult.next;
			current2 = current2.next;
		}

		return result;
	}
	public static void printList(Node head){
		Node current = head;
		while(current != null){
			System.out.print(current.data + "->");
			current = current.next;
		}
		System.out.println();
	}

	public static void printRec(Node head){
		if(head == null)
			return;
		System.out.print(head.data + "->");
		printRec(head.next);
	}

	public static void printReverse(Node head){
		if(head == null)
			return;

		printReverse(head.next);
		System.out.print(head.data+"->");
	}
	/*
	public static void reverseRec(Node node){
		if(node.next == null){
			//	head = node;
			return ;
		}

		reverseRec( node.next);		
		node.next.next = node;

	}
	 */
	public static boolean hasLoop(Node head){
		Node current = head, fastPtr = head;

		while(fastPtr != null && fastPtr.next != null){

			current = current.next;
			fastPtr = fastPtr.next.next;
			if(current == fastPtr)
				return true;
		}
		return false;
	}

	public static Node insertSorted(Node head, Node node){
		if( node == null)
			return head;
		if(head == null)
			return node;

		if(head.data > node.data){
			node.next = head;
			head = node;
		} else {
			Node current = head;
			while(current.next != null && current.next.data < node.data){	
				current = current.next;
			}
			node.next = current.next;
			current.next = node;
		}
		return head;
	}

	public static Node mergeSortedArray(Node h1, Node h2){
		Node c1 = h1, c2 = h2;
		Node nextInH2 = null;
		// TO DO
		while(c1 != null || c2 != null){
			if(c1.data == c2.data){
				nextInH2 = c2.next;
				c2.next = c1.next;
				c1.next = c2;

				c2 = nextInH2;
				c1 = c1.next;
			} else if (c1.data < c2.data){
				// insert
				c1 = c1.next;
			} else if (c1.data > c2.data);
		}
		return null;
	}

	public static int getSizeOfList(Node head){
		int count = 0;
		Node current = head;
		while(current != null){
			++count;
			current = current.next;
		}
		return count;
	}

	public static Node findIntersection(Node head1, Node head2){
		int c1 = getSizeOfList(head1);
		int c2 = getSizeOfList(head2);

		Node h1 = head1;
		Node h2 = head2;

		int diff = Math.abs(c1 - c2);
		if(c2 > c1)
			while(diff > 0){
				h2=h2.next;
				diff--;
			}
		else if(c1 > c2)
			while(diff > 0){
				h1 = h1.next;
				diff--;
			}

		while(h1 != null || h2 != null){
			if(h1 == h2)
				return h1;
			h1 = h1.next;
			h2 = h2.next;
		}
		return null;
	}

	public static Node reverseItera(Node head){
		Node current = head, next = head, prev = null;

		while(current != null){
			next = current.next;
			current.next = prev;
			prev = current;
			current = next;
		}

		head = prev;
		return head;
	}

	public static Node reverseRecr(Node oHead, Node head){

		if(oHead == null)
			return oHead;

		if(head.next == null){
			oHead = head;
			return oHead;
		}

		oHead = reverseRecr(oHead, head.next);

		head.next.next = head;
		head.next = null;

		return oHead;
	}

	/*public static Node mergeSortedLists(Node h1, Node h2){
		Node resultHead = null, lastNodeInResultPtr = null;
		Node c1 = h1, c2 = h2;
		while(c1 != null || c2 != null){
			if(c1 != null && c2 != null){
				if(c1.data <= c2.data){
					if(resultHead == null){
			//		resultHead = c1
					}
				}
			}
		}
	}*/

	public static boolean areIdentical(Node h1, Node h2){
		if(h1 == null && h2 == null)
			return true;

		/*	if((h1 == null && h2 != null) || (h1 != null && h2 == null))
			return false;*/

		if(h1 != null && h2 != null)
			return h1.data == h2.data && areIdentical(h1.next, h2.next);

		return false;
	}

	public static Node reverseIter(Node head){
		Node prev = null, current = head, next = head;

		while(current != null){
			next = current.next;
			current.next = prev;
			prev = current;
			current = next;
		}
		head = prev;
		return head;
	}

	/*	public static Node reverseRec(Node node){
		if(node == null)
			return head;

		if(node.next == null){
			head = node;
			return head;
		}

		reverseRec(node.next);
		node.next.next = node;
		node.next = null;

		return head;
	}*/

	public static Node removeDupUnsorted(Node head){
		if(head == null)
			return head;

		java.util.HashMap<Integer, Object> map = new java.util.HashMap<Integer, Object>();

		Node current = head;
		map.put(current.data, null);

		while(current.next != null){
			if(map.containsKey(current.next.data)){
				current.next = current.next.next;
			} else {
				map.put(current.next.data, null);
				current = current.next;
			}
		}

		return head;
	}

	public static void main(String args[]){
		Node head = new Node (0);
		head = addNode(head, new Node(5));
		head = addNode(head, new Node(2));
		head = addNode(head, new Node(4));
		head = addNode(head, new Node(5));
		head = addNode(head, new Node(5));

		printList(head);
		//head = removeDupUnsorted(head);
		//printList(head);
		//Node head2 = new Node(2);
		//printRec(head);
		//System.out.println("After reversing");
		head = reverseRecr(head,head);
		printList(head);
		//printReverse(head);
		//head = insertSorted(head, new Node(83));
		//printList(head);
		//System.out.println(areIdentical(head, head2));
	}
}
